execute as @a[gamemode=survival,nbt={Dimension:"rpg:pveworld"}] at @s run function rpg:players/rejoin
# scoreboard players reset @a[gamemode=survival,nbt={Dimension:"rpg:pveworld"}] 
execute as @a unless score @s park.uuid matches 0.. run function rpg:players/getuuid

execute in rpg:pveworld as @a[x=-1,y=-63,z=-1,distance=0..2,team=rpg] at @s run function rpg:players/died
execute in rpg:pveworld as @a[x=-1,y=-63,z=-1,distance=0..2,team=rpg_pvp] at @s run function rpg:players/died

scoreboard players add tick_rpg tick 1
execute unless entity @a[team=rpg] unless entity @a[team=rpg_pvp] run scoreboard players set tick_rpg tick -1

scoreboard players enable @a[team=rpg] rpg.save
execute as @a[scores={rpg.save=1..}] at @s run function rpg:players/savebag_force
scoreboard players reset @a[scores={rpg.save=1..}] rpg.save

scoreboard players enable @a[team=rpg] rpg.tp
execute as @a[scores={rpg.tp=1..}] at @s run function rpg:players/tppoint

execute as @a[team=rpg] at @s if block ~ ~ ~ nether_portal run function rpg:players/tryusetp

scoreboard players enable @a rpg.kill
execute as @a[scores={rpg.kill=1..}] at @s run function rpg:players/died
scoreboard players reset @a[scores={rpg.kill=1..}] rpg.kill


recipe take @a[team=rpg] *
recipe take @a[team=rpg_pvp] *
# 没人就别执行辣~

execute in rpg:pveworld if score tick_rpg tick matches 2 run function rpg:seconds/1
execute in rpg:pveworld if score tick_rpg tick matches 6 run function rpg:seconds/2
execute in rpg:pveworld if score tick_rpg tick matches 10 run function rpg:seconds/3
execute in rpg:pveworld if score tick_rpg tick matches 14 run function rpg:seconds/4
execute in rpg:pveworld if score tick_rpg tick matches 18.. run function rpg:seconds/5