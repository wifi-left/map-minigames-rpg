scoreboard players add @s rpg.spawn 0
title @s title "\u00a76传送门"

# 出生点
execute if score @s rpg.spawn matches ..0 run tp @s 406 -53 344 0 0
execute if score @s rpg.spawn matches ..0 run title @s subtitle "\u00a7a新手村"
# 精灵村
execute if score @s rpg.spawn matches 1 run tp @s -10 13 916 90 0
execute if score @s rpg.spawn matches 1 run title @s subtitle "\u00a7a精灵村"

execute at @s run playsound minecraft:entity.evoker.prepare_summon block @s ~ ~ ~ 1 1 1
# execute as @s at @s run particle minecraft:cloud ~ ~0.2 ~ 1 0 1 0 100 normal