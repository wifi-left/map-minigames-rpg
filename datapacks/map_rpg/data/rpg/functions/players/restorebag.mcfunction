tag @e remove rpg.tmp2
tag @a remove rpg.tmp
tag @s add rpg.tmp

execute as @e[type=marker,tag=rpg.playerbag] store result score @s board run data get entity @s data.uuid
execute as @e[type=marker,tag=rpg.playerbag] if score @s board = @a[limit=1,tag=rpg.tmp] park.uuid run tag @s add rpg.tmp2

data merge storage rpg:tmp {source:[]}
execute as @e[tag=rpg.tmp2] run data modify storage rpg:tmp source set from entity @s data.inventory
clear @s
execute if entity @e[tag=rpg.tmp2] run loot give @s loot rpg:item_store
# loot give @s mine -1 64 9 air{drop_contents:1b}
# /loot give @s mine -1 -64 9 air{drop_contents:1b}
# /loot give @s mine -1 -64 9 minecraft:air{drop_contents:1b}
execute if entity @e[tag=rpg.tmp2] run tellraw @s ["\u00a7a背包物品获取成功。"]
execute unless entity @e[tag=rpg.tmp2] in rpg:pveworld as @s run function rpg:players/summonbag
scoreboard players reset @e[type=marker,tag=rpg.playerbag]
tag @e remove rpg.tmp2
tag @a remove rpg.tmp