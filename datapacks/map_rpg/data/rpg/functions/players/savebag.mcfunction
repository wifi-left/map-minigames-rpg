tag @e remove rpg.tmp2
tag @a remove rpg.tmp
tag @s add rpg.tmp
execute as @e[type=marker,tag=rpg.playerbag] store result score @s board run data get entity @s data.uuid
execute as @e[type=marker,tag=rpg.playerbag] if score @s board = @a[limit=1,tag=rpg.tmp] park.uuid run tag @s add rpg.tmp2
# say @e[tag=rpg.tmp2]

# execute unless entity @e[tag=rpg.tmp2] in rpg:pveworld as @s run function rpg:players/summonbag
execute unless entity @e[tag=rpg.tmp2] in rpg:pveworld as @s run title @s actionbar "\u00a7c错误：自动保存物品失败。请尝试使用 \u00a76/trigger rpg.save \u00a7c保存。"


data modify entity @e[limit=1,tag=rpg.tmp2] data.inventory set from entity @s Inventory

# title @s actionbar ["\u00a7a背包物品保存成功。"]
scoreboard players reset @e[type=marker,tag=rpg.playerbag]
tag @e remove rpg.tmp2
tag @a remove rpg.tmp