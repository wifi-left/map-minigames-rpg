tag @e remove rpg.tmp2
tag @a remove rpg.tmp
tag @s add rpg.tmp
execute as @e[type=marker,tag=rpg.playerbag] store result score @s board run data get entity @s data.uuid
execute as @e[type=marker,tag=rpg.playerbag] if score @s board = @a[limit=1,tag=rpg.tmp] park.uuid run tag @s add rpg.tmp2
# say @e[tag=rpg.tmp2]

execute unless entity @e[tag=rpg.tmp2] in rpg:pveworld as @s run function rpg:players/summonbag
execute unless entity @e[tag=rpg.tmp2] in rpg:pveworld as @s run tellraw @s "\u00a7c错误：保存物品失败。请重试。"


data modify entity @e[limit=1,tag=rpg.tmp2] data.inventory set from entity @s Inventory

tellraw @s ["\u00a7a强制保存背包物品。"]
scoreboard players reset @e[type=marker,tag=rpg.playerbag]
tag @e remove rpg.tmp2
tag @a remove rpg.tmp