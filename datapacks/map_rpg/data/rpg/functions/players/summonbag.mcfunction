summon marker -1 -63 9 {Tags:["rpg.playerbag","rpg.new","rpg.tmp2"]}
# scoreboard players operation @e[tag=rpg.new] park.uuid = @s park.uuid
execute store result entity @e[tag=rpg.new,limit=1] data.uuid int 1 run scoreboard players get @s park.uuid
tag @e[tag=rpg.new] remove rpg.new
tellraw @s ["\n\u00a7e    您的物品将会被存储在系统内。\n\u00a76    但请注意：如果您直接重置地图，地图数据将会消失！\n"]