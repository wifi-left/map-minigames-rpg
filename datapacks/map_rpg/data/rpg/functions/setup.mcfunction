scoreboard objectives add tick dummy
scoreboard objectives add board dummy
tellraw @a ["\u00a7e\u00a7l「MiniGames RPG 附加包」\u00a7a加载成功！"]
scoreboard objectives add park.uuid dummy "UID"
scoreboard objectives add rpg.spawn dummy "重生点"
scoreboard objectives add rpg.tp trigger "\u00a76传送到"
scoreboard objectives add rpg.save trigger "\u00a76强制保存物品"
scoreboard objectives add rpg.kill trigger "\u00a7c强制传送到传送点"

team add rpg "RPG"
team add rpg_pvp "RPG (PVP Enabled)"
team modify rpg_pvp friendlyFire true
team modify rpg_pvp deathMessageVisibility always
team modify rpg deathMessageVisibility always
team modify rpg friendlyFire false
team modify rpg color gray
team modify rpg_pvp color gray
team modify rpg prefix "\u00a7a[F] \u00a7eRPG \u00a78| "
team modify rpg_pvp prefix "\u00a7c[P] \u00a7eRPG \u00a78| "

execute in rpg:pveworld run forceload remove 0 -1 -1 9
execute in rpg:pveworld run forceload add 0 -1 -1 9